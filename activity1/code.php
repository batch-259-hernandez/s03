<?php
class Person
{

    //properties (instead of variables)
    // Characteristics of an object
    public $firstName;
    public $middleName;
    public $lastName;

    // Constructor Function
    // A constructor allows us to initialize an object's properties upon creation of the object
    public function __construct($firstName, $middleName, $lastName)
    {
        $this->firstName = $firstName;
        $this->middleName = $middleName;
        $this->lastName = $lastName;
    }

    //Methods
    // An action that object can perform
    public function printName()
    {
        return "Your full name is $this->firstName $this->middleName $this->lastName";
    }
}

// Inheritance and Polymorphism
// Inheritance - The derive classes are allowed to inherit properties and methods from a specified base class.
// The "extends" keyword is used to derive a class from another class.
// A derive/class class has all the properties and methods of the parent class.
// PARENT CLASS => BUILDING
// CHILD CLASS => CONDOMINIUM
class Developer extends Person
{
    // Polymorphism - Methods inherited by a child/derived class can be overriden to have a behavior different from the method of the base/parent class.
    public function printName()
    {
        return "Your name is $this->firstName $this->middleName $this->lastName and you are a developer";
    }
}

class Engineer extends Person
{
    // Polymorphism - Methods inherited by a child/derived class can be overriden to have a behavior different from the method of the base/parent class.
    public function printName()
    {
        return "You are an Engineer $this->firstName $this->middleName $this->lastName";
    }
}

$person = new Person('Senku', '', 'Ishigami');
$developer = new Developer('John', 'Finch', 'Smith');
$engineer = new Engineer('Harold', 'Myers', 'Reese');
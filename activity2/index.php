<?php require_once "./code.php" ?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>S03B: Activity2</title>
</head>

<body>
    <h1>Building</h1>


    <p> <?php echo 'The name of the building is ' . $building->getName(); ?> </p>
    <p> <?php echo 'The ' . $building->getName() . ' has ' . $building->getFloor() . ' floors.'; ?> </p>

    <p> <?php echo 'The ' . $building->getName() . ' is located at ' . $building->getAddress(); ?> </p>

    <?php $building->setName('Caswyn Complex'); ?>
    <p> <?php echo 'The name of the building has changed to ' . $building->getName(); ?> </p>


    <h1>Condominium</h1>

    <?php $condominium->setName('Enzo Condo'); ?>
    <p> <?php echo 'The name of the condominium is ' . $condominium->getName(); ?> </p>
    <p> <?php echo 'The ' . $condominium->getName() . ' has ' . $condominium->getFloor() . ' floors.'; ?> </p>

    <p> <?php echo 'The ' . $condominium->getName() . ' is located at ' . $condominium->getAddress(); ?> </p>

    <?php $condominium->setName('Enzo Tower'); ?>
    <p> <?php echo 'The name of the condominium has changed to ' . $condominium->getName(); ?> </p>










</body>

</html>
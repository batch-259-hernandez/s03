<?php


// Objects as Variables

$buildingObj = (object)[
    'name' => 'Caswynn building',
    'floor' => 8,
    'address' => 'Brgy. Sacred Heart, Quezon City, Philippines'
];

$buildingObj2 = (object)[
    'name' => 'GMA Network',
    'floor' => 20,
    'address' => 'Brgy. Sacred Heart, Quezon City, Philippines'
];

// Objects from classes
class Building
{

    //properties (instead of variables)
    // Characteristics of an object
    public $name;
    public $floor;
    public $address;

    // Constructor Function
    // A constructor allows us to initialize an object's properties upon creation of the object
    public function __construct($name, $floor, $address)
    {
        $this->name = $name;
        $this->floor = $floor;
        $this->address = $address;
    }

    //Methods
    // An action that object can perform
    public function printName()
    {
        return "The name of the building is $this->name";
    }
}

// Inheritance and Polymorphism
// Inheritance - The derive classes are allowed to inherit properties and methods from a specified base class.
// The "extends" keyword is used to derive a class from another class.
// A derive/class class has all the properties and methods of the parent class.
// PARENT CLASS => BUILDING
// CHILD CLASS => CONDOMINIUM
class Condomium extends Building
{
    // Polymorphism - Methods inherited by a child/derived class can be overriden to have a behavior different from the method of the base/parent class.
    public function printName()
    {
        return "The name of the condominium is $this->name";
    }
}



$building = new Building('Caswynn Buiding', 8, 'Timog Ave. Quezon City, Philippines');

$condominium = new Condomium('Enzo Condo', 5, 'Buenja Ave, Makati City, Philippines');